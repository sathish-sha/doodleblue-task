import React, { Component } from 'react';
import ContactForm from './contactform';
import ChatBox from './ChatBox';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class ContactList extends Component {
    state= {
        currentIndex: -1,
        contactlists:this.returnContact(),
        
    }
  
    returnContact () {
        if(localStorage.getItem('contacts')===null)
            localStorage.setItem('contacts',JSON.stringify([]))
            return JSON.parse(localStorage.getItem('contacts'))
    }
    onAddOrEdit = (data) => {
        var contactlists = this.returnContact()
        if(this.state.currentIndex === -1)
        contactlists.push(data)
        else 
        contactlists[this.state.currentIndex]=data
        localStorage.setItem('contacts',JSON.stringify(contactlists))
        this.setState({contactlists})
        toast.success("Contact Added / Updated Successfully !");
    }
    handleEdit= index => {
        this.setState({
            currentIndex:index
        })
    }
    handleDelete= index => {
        var contactlists = this.returnContact()
        contactlists.splice(index,1)
        localStorage.setItem('contacts',JSON.stringify(contactlists))
        this.setState({contactlists,currentIndex:-1})
        toast.error("Contact Deleted Successfully !");
    }
    
  render() {
    
    toast.configure({
        autoClose: 4000,
        draggable: false,
        //etc you get the idea
      });
    return (
        <div className="row pl-0 pr-0">
        <div className="col-lg-12 pl-0 pr-0">
            <div className="col-md-4 float-left pl-0 pr-0" >
            <ContactForm 
            onAddOrEdit={this.onAddOrEdit}
            currentIndex={this.state.currentIndex} 
            contactlists={this.state.contactlists}/>
            </div>
            <div className="col-md-8 float-left pr-0" id="msform" >
              <div className="card border-0">
                <div className="card-body ">
                    <div className="card-title">
                        <h2 className="fs-title">contact List</h2>
                    </div>
                    <div className="table-responsive table-height">
                        <table className="table table-striped table-bordered " >
                            <thead className="fs-subtitle">
                                <tr>
                                <th>S.No</th>
                                <th>Contact Name</th>
                                <th>Contact Number</th>
                                <th>Action</th>
                                <th>Chat</th>
                                </tr>
                            </thead>
                            <tbody >
                                {this.state.contactlists.map((item, index) => {
                                    return<tr key={index}>
                                    <td>{index+1}</td>
                                <td>{item.Cname}</td>
                                    <td>{item.PhoneNo}</td>
                                    <td><button className="btn btn-sm btn-warning text-white"  onClick={()=> this.handleEdit(index)}><i className="fa fa-edit"/></button>
                                    <button className="btn btn-sm btn-danger text-white ml-2"  onClick={this.toggle}><i className="fa fa-trash"/></button></td>
                                    <td className="w-50"><button className="btn btn-sm btn-primary" ><i className="fa fa-comments"/></button></td>
                                    </tr>
                                })}
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <ChatBox/>
    </div>  
    )
  }
}


export default ContactList;
