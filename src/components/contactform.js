import React, { Component } from 'react';
class ContactForm extends Component {
  state = {
    ...this.returnStateObject()
  }
  returnStateObject() {
    if(this.props.currentIndex === -1)
    return {
      Cname : '',
    PhoneNo : ''
    }
    else
    return this.props.contactlists[this.props.currentIndex]
  }
  componentDidUpdate(prevProps){
    if(prevProps.currentIndex !== this.props.currentIndex || prevProps.contactlists.length !== this.props.contactlists.length)
    this.setState({...this.returnStateObject() })
  }
  handleInputChange = e => {
    this.setState({
      [e.target.name]:e.target.value
    }) 
  }
  handleSubmit = e => {
    e.preventDefault()
    this.props.onAddOrEdit(this.state)
  }
  render() {
    return (
        
            <form id="msform" name="registration" autoComplete="off" onSubmit={this.handleSubmit}>
            
            <fieldset className="form1 pt-5 pb-5" id="form1">
              <h2 className="fs-title">Contact Details</h2>
              <input type="text" id="Cname" name="Cname" placeholder="Contact Name" value={this.state.Cname} onChange={this.handleInputChange} required/>
              <input type="tel" id="PhoneNo" name="PhoneNo" placeholder="Contact Number" value={this.state.PhoneNo} onChange={this.handleInputChange} required/>
              <input type="submit" name="Add" className="next action-button" defaultValue="ADD" id="ADD" />
            </fieldset>
          </form>
    )
  }
}


export default ContactForm;
