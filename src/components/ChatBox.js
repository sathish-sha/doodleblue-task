import React, { Component } from 'react';
class ChatBox extends Component {
    render() {
        return (
                <div className="col-lg-12 pl-0 pr-0 mt-3 mb-3">
                <section className="msger">
            <header className="msger-header">
              <div className="msger-header-title">
                <i className="fas fa-comment-alt"></i> SimpleChat
              </div>
              <input type="checkbox" checked data-toggle="toggle" data-on="Sender" data-off="Receiver" data-onstyle="success" data-offstyle="danger" />
              <div className="msger-header-options">
                <span><i className="fa fa-close"></i></span>
              </div>
            </header>
          
            <main className="msger-chat">
              <div className="msg left-msg">
                <div
                 className="msg-img"
                 style={{BackgroundImage: "url(https://image.flaticon.com/icons/svg/327/327779.svg)"}}
                ></div>
          
                <div className="msg-bubble">
                  <div className="msg-info">
                    <div className="msg-info-name">BOT</div>
                    <div className="msg-info-time">12:45</div>
                  </div>
          
                  <div className="msg-text">
                    Hi, welcome to SimpleChat! Go ahead and send me a message. 😄
                  </div>
                </div>
              </div>
          
              <div className="msg right-msg">
                <div
                 className="msg-img"
                 style={{BackgroundImage: "url(https://image.flaticon.com/icons/svg/145/145867.svg)"}}
                ></div>
          
                <div className="msg-bubble">
                  <div className="msg-info">
                    <div className="msg-info-name">Sajad</div>
                    <div className="msg-info-time">12:46</div>
                  </div>
          
                  <div className="msg-text">
                    You can change your name in JS section!
                  </div>
                </div>
              </div>
            </main>
          
            <form className="msger-inputarea">
              <input type="text" className="msger-input" placeholder="Enter your message..." />
              <button type="submit" className="msger-send-btn">Send</button>
            </form>
          </section>
                </div>
            
        )
      }
    }
    
    
    export default ChatBox;
    