$("#ADD").click(function() {
    var form = $("#msform");
    form.validate({
      rules: {
        Cname: {
            required: true,
            min: 3
        },
        phone: {
          required:true,
        minlength:10,
        maxlength:10,
        digits:true
        }
      },
      messages: {
        Cname: {
          required: "This is required.",
          minlength: "Too short."

        },
        phone:{
          required:   "Phone number is requied",
          minlength:  "Please enter 10 digit mobile number",
          maxlength:  "Please enter 10 digit mobile number",
          digits:   "Only numbers are allowed in this field"
        }
      }
    });
    
  });